module webhook-alarm

go 1.16

require (
	github.com/ajg/form v1.5.1 // indirect
	github.com/bitly/go-simplejson v0.5.0
	github.com/bmizerany/assert v0.0.0-20160611221934-b7ed37b82869 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/imkira/go-interpol v1.1.0 // indirect
	github.com/kataras/iris/v12 v12.1.8
	github.com/moul/http2curl v1.0.0 // indirect
	github.com/pyroscope-io/pyroscope v0.3.1
	github.com/streadway/amqp v1.0.0
	github.com/valyala/fasthttp v1.31.0 // indirect
	github.com/xeipuuv/gojsonschema v1.2.0 // indirect
	github.com/yalp/jsonpath v0.0.0-20180802001716-5cc68e5049a0 // indirect
	github.com/yudai/gojsondiff v1.0.0 // indirect
	github.com/yudai/golcs v0.0.0-20170316035057-ecda9a501e82 // indirect
	go.mongodb.org/mongo-driver v1.8.3
	go.uber.org/zap v1.17.0
	golang.org/x/text v0.3.7 // indirect
	golang.org/x/time v0.0.0-20211116232009-f0f3c7e86c11
	google.golang.org/protobuf v1.28.0 // indirect
)
