# webhook-alarm

#### 介绍

    sentry/skywalking报警webhook
    监听8888端口
    请求参数sent_purpose={sentry/skywalking}
    使用sentry/skywalking tags中server_name字段为分组判断依据
    服务可以存在于多个组中,组名支持正则

#### 配置说明
    Sentry配置：
        webhook中配置即可，需应用打tag
---
    Skywalking配置样例：

    rules:
        china_service__p99_rule:
            metrics-name: dschina_service_p99
            op: ">"
            threshold: 2000
            period: 10
            count: 3
            silence-period: 20
            tags:
                server_name: dschina-all
            message: Response time of service {name} is more than 2000ms in 3 minutes of last 10 minutes.
        china_service_sla_rule:
            metrics-name: dschina_service_sla
            op: "<"
            threshold: 8000
            period: 10
            count: 3
            silence-period: 20
            tags:
                server_name: dschina-all
            message: Successful rate of service {name} is lower than 80% in 3 minutes of last 10 minutes
    webhooks:
        - http://10.1.8.66:8888/?sent_purpose=skywalking
#### 使用说明

    样例：./webhook_alarm -s 127.0.0.1:4040  -rate 100 -scope 

    [root@golang src]# ./webhook_alarm --help
    
    Usage of ./webhook_alarm:

    -logType string
        设置日志类型，container: 控制台日志，否则日志输出到控制台及文件
 
    -mgaddr string
        设置mongodb连接串 (default "mongodb://localhost:27017")

    -mqaddr string
        设置rabbitmq地址 (default "localhost")
 
    -mquser string
        设置rabbitmq用户 (default "admin")

    -mqpass string
        设置rabbitmq密码 (default "admin")
 
    -mqport int
        设置mq端口 (default 5672)

    -mqvhost string
        设置vhost (default "/alarm")

    -rate int
        设置每秒发送频率 (default 100)
  
    -s string
        设置pyroscope服务端地址 (default "localhost:4040")

    -scope
        性能监测开关

    [root@golang src]# 
