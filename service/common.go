package service

import (
	"context"
	"github.com/bitly/go-simplejson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
	OrgRate "golang.org/x/time/rate"
	"sync"
	//kelleyRabbimqPool "gitee.com/tym_hmm/rabbitmq-pool-go"
	"webhook-alarm/rabbitmqpool"
)

var welcome = `
 /~~~~~~~~\_/~~~~~~~\__/~~~~~~~\__/~~\__/~~\_
 ___/~~\____/~~\__/~~\_/~~\__/~~\_/~~\__/~~\_
 ___/~~\____/~~\__/~~\_/~~\__/~~\_/~~~~~~~~\_
 ___/~~\____/~~\__/~~\_/~~\__/~~\_/~~\__/~~\_
 ___/~~\____/~~~~~~~\__/~~~~~~~\__/~~\__/~~\_
`
var (
    MyHandler = &Handler{}
    contextValue = context.Background()

    ZapLog *zap.Logger
    token map[string]string
    GroupMap map[string]interface{}
    limiteRate OrgRate.Limit
    limit *OrgRate.Limiter
    oncePool,onceConsumePool sync.Once
    instanceRPool,instanceConsumePool *kelleyRabbimqPool.RabbitPool
    MongodbClient *mongo.Client
)

type Handler struct {
	wechat *simplejson.Json
}
 
type SentryMsg struct {
	TraceId       string
	Project       string
	Issue_service string
	Issue_id      string
	Issue_message string
	Issue_culprit string
	Issue_url     string
}

type SkyMsg struct {
	TraceId      string
	Scope        string
	Name         string
	RuleName     string
	AlarmMessage string
}
