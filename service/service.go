package service

import (
	"github.com/kataras/iris/v12"
	"go.uber.org/zap"
	"strconv"
	"time"
)

func Service() {

	app := iris.New()

	app.Get("/", func(ctx iris.Context) {
		ctx.Writef(welcome)
	})

	app.Get("/wqfgddwq", func(ctx iris.Context) {
		token, _ := CreateJWTToken("0001", "admin", "mu tong qi huang niu")
		ctx.Writef("auth=" + token)
	})

	app.Post("/post", func(ctx iris.Context) {
		ctx.Writef("Hello from %s", ctx.RemoteAddr())
		body, _ := ctx.GetBody()
		//logNew.Info("Post: ", string(body))
		ZapLog.Info("webhook-alarm",
			zap.Any("PostBody:", string(body)),
		)
	})

	app.Post("/", Middleware, func(ctx iris.Context) {
		ctx.Writef("Hello from %s", ctx.RemoteAddr())
		body, _ := ctx.GetBody()
		timing := strconv.FormatInt(time.Now().UnixNano(), 36)
		MyHandler.ServeHTTP(body, ctx.FormValue("sent_purpose"), timing)
	})

	app.Run(iris.Addr(":8888"))
}
