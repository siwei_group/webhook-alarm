package main

import (
	"webhook-alarm/service"
)

func main() {
	service.Parse()
	service.Service()
}
